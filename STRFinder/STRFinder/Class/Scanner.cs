﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace STRFinder.Class
{
    class Scanner
    {
        private string _filePath;
        public string filePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    filePath = @"\";
                }
                else
                    _filePath = filePath;
            }
        }

        private List<string> _types;
        public List<string> Types
        {
            get
            {
                return _types;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                if(value.Count == 0)
                {
                    return;
                }

                List<string> okTypes = new List<string>();
                foreach (string type in Types)
                {
                    if(getAllTypes().Count(tp => tp == type) == 1)
                    {
                        okTypes.Add(type);
                    }
                    else
                    {
                        Console.WriteLine("Not supported file format: " + type +", ignoring.");
                    }
                }

                _types = okTypes;
            }
        }

        public void addType(string type)
        {
            if (_types.Count(tp => tp == type) == 0)
            {
                if (getAllTypes().Count(tp => tp == type) == 1)
                {
                    _types.Add(type);
                }
                else
                {
                    Console.WriteLine("Not supported file format: " + type + ", ignoring.");
                }
            }
        }

        public Scanner()
        {
            _filePath = @"\";
            _types = getAllTypes();

        }

        public Scanner(string filePath = null)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                _filePath = @"\";
            }
            else
                _filePath = filePath;

            _types = new List<string>();
        }

        public Scanner(string filePath = null, List<string> types = null)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                _filePath = @"\";
            }
            else
                _filePath = filePath;

            if (types == null)
                _types = new List<string>();
            else
                _types = types;
        }

        //Ustawia ścieżkę do skanowania plików
        public void setPath(string filePath = null)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                _filePath = @"\";
                Console.WriteLine("Scanning by default patch");
            }
            else
                _filePath = filePath;
        }

        //Tworzy tablicę plików FileInfo[] na podstawie parametrów typów plików i ścieżki dostępu
        //Standardowa metoda GetFiles FileInfo[] potrafi przyjąć wyłącznie jeden typ pliku
        public FileInfo[] getFilesByTypes(List<string> types)
        {
            DirectoryInfo d = new DirectoryInfo(_filePath);
            List<FileInfo> fileL = new List<FileInfo>();
            foreach (string type in types)
            {
                FileInfo[] Files = d.GetFiles("*." + type); 
                foreach (FileInfo file in Files)
                {
                    fileL.Add(file);
                }
            }
            return fileL.ToArray();
        }

        //Wyciąga z pliku MP3 dane o utworze(tagi)
        public void getMp3Tags(FileStream fs)
        {
            Track tag = new Track();
            fs.Seek(-128, SeekOrigin.End);
            fs.Read(tag.bTAGID, 0, tag.bTAGID.Length);
            fs.Read(tag.bTitle, 0, tag.bTitle.Length);
            fs.Read(tag.bArtist, 0, tag.bArtist.Length);
            fs.Read(tag.bAlbum, 0, tag.bAlbum.Length);
            fs.Read(tag.bYear, 0, tag.bYear.Length);
            fs.Read(tag.bComment, 0, tag.bComment.Length);
            fs.Read(tag.bGenre, 0, tag.bGenre.Length);
            string theTAGID = Encoding.Default.GetString(tag.bTAGID);
            Track track = new Track();

            if (theTAGID.Equals("TAG"))
            {
                tag.convertByteToString();

                Console.WriteLine("===============================");
                Console.WriteLine("Title: " + tag.Title);
                Console.WriteLine("Artist: " + tag.Artist);
                Console.WriteLine("Album: " + tag.Album);
                Console.WriteLine("Year: " + tag.Year);
                Console.WriteLine("Comment: " + tag.Comment);
                Console.WriteLine("Genre: " + tag.Genre);
                Console.WriteLine("===============================");
            }
            else
            {
                Console.WriteLine("===============================");
                Console.WriteLine("EMPTY FILEINFO");
                Console.WriteLine("===============================");
            }
        }

        public void getFlacTags()
        {
            Console.WriteLine("===============================");
            Console.WriteLine("Flac is not supported yet.");
            Console.WriteLine("===============================");
        }


        //Skanuje wszystkie dostepne utwory dla wskazanych typów, wszystkich dostepnych typów(gdy types == null)
        public TrackList scanAll()
        {
            Console.WriteLine("Scanning files in " + _filePath + "..");
            TrackList trackList = new TrackList();

            DirectoryInfo d = new DirectoryInfo(_filePath);
            FileInfo[] Files = getFilesByTypes(_types);

            foreach(FileInfo file in Files)
            {
                Console.WriteLine(file.Name);
            }

            Console.WriteLine("Scanning data..");
            foreach (FileInfo file in Files)
            {
                using (FileStream fs = File.OpenRead(_filePath + @"\" + file.Name))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Track: " + file.Name);

                    if (fs.Length >= 128)
                    {
                        switch(file.Extension)
                        {
                            case ".mp3":
                                getMp3Tags(fs);
                                break;

                            case ".flac":
                                getFlacTags();
                                break;

                            default:
                                Console.WriteLine("===============================");
                                Console.Write("EMPTY FILEINFO");
                                Console.WriteLine("===============================");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("===============================");
                        Console.Write("NO INFO");
                        Console.WriteLine("===============================");
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Complete.");

            return trackList;
        }

        //Pobieranie wszystkich dostępnych rozszerzeń plików do przeskanowania
        //TODO: pobieranie z pliku konfiguracyjnego XML
        List<string> getAllTypes()
        {
            List<string> types = new List<String>();
            types.Add("mp3");
            types.Add("flac");
            return types;
        }
    }
}
