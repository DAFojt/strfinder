﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STRFinder.Class
{
    class Track
    {
        public string Title;
        public string Artist;
        public string Album;
        public string Year;
        public string Comment;
        public string Genre;



        public byte[] bTAGID = new byte[3];      //  3
        public byte[] bTitle = new byte[30];     //  30
        public byte[] bArtist = new byte[30];    //  30 
        public byte[] bAlbum = new byte[30];     //  30 
        public byte[] bYear = new byte[4];       //  4 
        public byte[] bComment = new byte[30];   //  30 
        public byte[] bGenre = new byte[1];      //  1

        public void convertByteToString()
        {
            Title = Encoding.Default.GetString(bTitle);
            Artist = Encoding.Default.GetString(bArtist);
            Album = Encoding.Default.GetString(bAlbum);
            Year = Encoding.Default.GetString(bYear);
            Comment = Encoding.Default.GetString(bComment);
            Genre = Encoding.Default.GetString(bGenre);
        }
    }
}
